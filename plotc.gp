reset

set terminal epslatex color
set output 'Proftheo.tex'

set xlabel 'x [m]'
set ylabel '$\frac{\text{C}}{\text{C}_0}$'

set yrange [-0.1:1.1]
set xrange [-20:20]

f(x)=0.5*(1-erf(x/(sqrt(4*(4*10**(-10))*10**(9)))))
g(x)=0.5*(1-erf(x/(sqrt(4*(4*10**(-10))*10**(10)))))
h(x)=0.5*(1-erf(x/(sqrt(4*(4*10**(-10))*10**(11)))))
i(x)=0.5*(1-erf(x/(sqrt(4*(4*10**(-10))*10**(12)))))
j(x)=0.5*(1-erf(x/(sqrt(4*(4*10**(-10))*10**(13)))))
k(x)=0.5*(1-erf(x/(sqrt(4*(4*10**(-10))*1))))


p k(x) t 't$_0$ = 0.0s' lt -1 , f(x) t 't$_1$ = 10$^{9}s$' lt -1 lc 1, g(x) t 't$_2$ = 10$^{10}s$' lt -1 lc 3, h(x) t 't$_3$ = 10$^{11}$s' lt -1 lc 2, i(x) t 't$_4$ = 10$^{12}$s' lt -1 lc 4
set output
!epstopdf Proftheo.eps
