reset

set terminal epslatex color

f(x)=m*x
g(x)=a*x
set output 'zeitplot1.tex
set xlabel 'Zeit t [s]'
set ylabel 'Diffusionstrecke$^2$ [m$^2$]'
set key top left

set fit logfile 'zeit1.log'
fit f(x) "zeit16.dat" u 1:($2**2) via m
set fit logfile 'zeit2.log'
fit g(x) "zeit32.dat" u 1:($2**2) via a
p f(x) t 'lineare Regression' lt 1 lc 2, "zeit16.dat" u 1:($2**2):3 w e t '$\frac{c_0}{16}$', g(x) t 'lineare Regression' lt 1 lc 4, "zeit32.dat" u 1:($2**2):3 w e t '$\frac{c_0}{32}$'
set output
!epstopdf zeitplot1.eps
