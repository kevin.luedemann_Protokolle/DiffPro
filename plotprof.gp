reset

set terminal epslatex color

set xlabel 'x [m]'
set ylabel '$\frac{\text{C}}{\text{C}_0}$'

f(x)=0.5*(1-erf(x/sqrt(4*(4*10**(-10))*2475)))
g(x)=0.5*(1-erf(x/sqrt(4*(4*10**(-10))*5785)))

set xtics 0,0.001,0.006

set xrange [-0.0002:0.006]
set output 'profiel1.tex'
p "prof1.dat" u (($4-470)/100000):3 t 'Messwerte', f(x) t 't = 41 Minuten'
set output
!epstopdf profiel1.eps

set xrange [-0.0001:0.004]
set output 'profiel2.tex'
p "prof2.dat" u (($4-797)/100000):3 t 'Messwerte', g(x) t 't = 96 Minuten'
set output
!epstopdf profiel2.eps
